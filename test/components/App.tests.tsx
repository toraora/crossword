/* tslint:disable */
import React from 'react';
import { createRenderer } from 'react-test-renderer/shallow';

import App from '../../src/components/App';

describe('App', () => {
  it('renders expected HTML', () => {
    const app = render({});
    expect(app).toEqual(
      <div className="container">
        <h1>Hello!</h1>
      </div>
    );
  });

  function render(state: any) {
    state = state;
    const shallowRenderer = createRenderer();
    shallowRenderer.render(<App />);
    return shallowRenderer.getRenderOutput();
  }
});
