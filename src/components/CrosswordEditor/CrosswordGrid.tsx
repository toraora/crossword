import React from 'react';

import { Crossword, GridEntry, GridEntryDisplayType } from '../../lib/Crossword';

interface CrosswordGridEntryProps {
    value: string;
    entry: GridEntry;
    row: number;
    col: number;
    handleClick: any;
    isSelected: boolean;
    isSelectedSquare: boolean;
}

class CrosswordGridEntry extends React.PureComponent<CrosswordGridEntryProps, {}> {
    maybeRenderClueLabel = () => {
        if (this.props.entry.clueLabel) {
            return <div className="cw-cluelabel">
                <span>{this.props.entry.clueLabel}</span>
            </div>;
        }
    }

    handleClick = () => {
        const props = this.props;
        props.handleClick(props.row, props.col);
    }

    render() {
        let className = 'cw-square';
        const props = this.props;
        if (props.isSelectedSquare) {
            className += ' selectedPrimary';
        }
        else if (props.isSelected) {
            className += ' selected';
        }
        else if (!props.entry.isFillable) {
            className += ' unfillable';
        }

        let innerClassName = '';
        if (props.entry.displayType === GridEntryDisplayType.Circle) {
            innerClassName += ' cw-gridsquare-circle';
        }
        if (props.entry.displayType === GridEntryDisplayType.Shaded) {
            innerClassName += ' cw-gridsquare-shaded';
        }

        return <div
            className={className}
            onClick={this.handleClick}
            style={{ gridRow: props.row + 1, gridColumn: props.col + 1 }}
        >
            <div className={`cw-gridsquare ${innerClassName}`} />
            {this.maybeRenderClueLabel()}
            <span>{props.value}</span>
        </div>;
    }
}

interface CrosswordGridProps {
    thePuzzle: Crossword;
    cursorRow: number;
    cursorCol: number;
    isHorizontal: boolean;
    handleClick: any;
}

const CrosswordGrid = (props: CrosswordGridProps) => {
    const puzzle = props.thePuzzle;
    const selectedClue = props.thePuzzle.getClue(
        props.cursorRow,
        props.cursorCol,
        props.isHorizontal
    );
    const gridSquares: any[] = [];
    for (let x = 0; x < puzzle.height; x++) {
        for (let y = 0; y < puzzle.width; y++) {
            gridSquares.push(<CrosswordGridEntry
                value={puzzle.gridValues[x][y]}
                entry={puzzle.grid[x][y]}
                row={x}
                col={y}
                key={x * puzzle.width + y}
                handleClick={props.handleClick}
                isSelected={(props.isHorizontal
                    ? puzzle.grid[x][y].acrossClue
                    : puzzle.grid[x][y].downClue
                ) === selectedClue}
                isSelectedSquare={x === props.cursorRow && y === props.cursorCol}
            />);
        }
    }

    return <div>
        <div className="cw-current-clue">
            <b>{selectedClue.num}{props.isHorizontal ? 'A' : 'D'}</b>
            <span>&nbsp;&nbsp;{selectedClue.clue}</span>
        </div>
        <div className="cw-outer">
            <div className="cw-container" style={{ fontSize: `${30 / puzzle.width}vw` }}>
                {gridSquares}
            </div>
        </div>
    </div>;
};

// export { CrosswordGridEntry };
export { CrosswordGrid };
