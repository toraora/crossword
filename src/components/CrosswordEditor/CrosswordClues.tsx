import React from 'react';

import { Crossword } from '../../lib/Crossword';
import { PuzzleClue } from '../../lib/PuzzleSpecification';

interface CrosswordClueRowProps {
    clue: PuzzleClue;
    isSelected: boolean;
    isSelectedCross: boolean;
    isRelated: boolean;
    isHorizontal: boolean;
    handleClick: any;
}

class CrosswordClueRow extends React.PureComponent<CrosswordClueRowProps, {}> {
    handleClick = () => {
        const props = this.props;
        props.handleClick(props.clue.cell, props.isHorizontal);
    }

    render() {
        const props = this.props;
        let className = 'cw-clues-row';
        let classNameSecondary = 'cw-clues-secondary';

        if (props.isSelected) {
            className += ' selected';
        }
        if (props.isRelated) {
            className += ' related';
        }
        if (props.isSelectedCross) {
            classNameSecondary += ' selected';
        }

        return <div className={className} onClick={this.handleClick}>
            <div className={classNameSecondary} />
            <div className="cw-clues-primary">
                <div>{props.clue.num}</div>
                <div>{props.clue.clue}</div>
            </div>
        </div>;
    }
}

interface CrosswordCluesProps {
    thePuzzle: Crossword;
    cursorRow: number;
    cursorCol: number;
    isHorizontal: boolean;
    handleClick: any;
}

interface CrosswordCluesColumnProps {
    thePuzzle: Crossword;
    clues: PuzzleClue[];
    cursorRow: number;
    cursorCol: number;
    isHorizontal: boolean;
    handleClick: any;
    label: string;
}

class CrosswordCluesColumn extends React.Component<CrosswordCluesColumnProps, {}> {
    componentDidUpdate() {
        const elements = document.querySelectorAll('.cw-clues-row.selected,.cw-clues-secondary.selected');
        for (let i = 0; i < elements.length; i++) {
            const el = elements.item(i);
            el.scrollIntoView({
                behavior: 'instant',
                block: 'start',
                inline: 'nearest'
            });
        }
    }

    render() {
        const props = this.props;
        return <div className="cw-clues-column">
            <p className="cw-clues-column-label">{this.props.label}</p>
            <div className="vscroll">
                {this.props.clues.map(c => <CrosswordClueRow
                    // entry={props.thePuzzle.grid[props.cursorRow][props.cursorCol]}
                    clue={c}
                    isSelected={props.thePuzzle.getClue(
                        props.cursorRow,
                        props.cursorCol,
                        props.isHorizontal
                    ) === c}
                    isSelectedCross={props.thePuzzle.getClue(
                        props.cursorRow,
                        props.cursorCol,
                        !props.isHorizontal
                    ) === c}
                    isRelated={false}
                    isHorizontal={props.label === 'ACROSS'} // JANK ALERT
                    key={c.cell}
                    handleClick={props.handleClick}
                />)}
            </div>
        </div>;
    }
}

class CrosswordClues extends React.Component<CrosswordCluesProps, {}> {
    render() {
        const props = this.props;
        return <div className="cw-clues">
            <CrosswordCluesColumn
                thePuzzle={props.thePuzzle}
                clues={props.thePuzzle.puzzle.clues_h}
                cursorRow={props.cursorRow}
                cursorCol={props.cursorCol}
                isHorizontal={props.isHorizontal}
                handleClick={props.handleClick}
                label="ACROSS"
            />
            <CrosswordCluesColumn
                thePuzzle={props.thePuzzle}
                clues={props.thePuzzle.puzzle.clues_v}
                cursorRow={props.cursorRow}
                cursorCol={props.cursorCol}
                isHorizontal={props.isHorizontal}
                handleClick={props.handleClick}
                label="DOWN"
            />
        </div>;
    }
}

export { CrosswordClues };
