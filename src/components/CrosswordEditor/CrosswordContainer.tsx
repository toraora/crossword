import React from 'react';

import { Crossword } from '../../lib/Crossword';
import { StateSync } from '../../lib/StateSync';

import { CrosswordGrid } from './CrosswordGrid';
import { CrosswordClues } from './CrosswordClues';

interface Direction {
    dx: number;
    dy: number;
}

function getNextFillable(
    row: number,
    col: number,
    isHorizontal: boolean,
    thePuzzle: Crossword,
    dir: Direction,
    shouldJump: boolean,
    shouldSkipFilled: boolean
): {
    nextRow: number,
    nextCol: number,
    isHorizontal: boolean
} {
    if (dir.dx === 0 && isHorizontal === false) {
        return {
            nextRow: row,
            nextCol: col,
            isHorizontal: true
        };
    }
    else if (dir.dy === 0 && isHorizontal === true) {
        return {
            nextRow: row,
            nextCol: col,
            isHorizontal: false
        };
    }
    const origRow = row;
    const origCol = col;
    while (true) {
        row = row + dir.dx;
        col = col + dir.dy;
        if (row < 0 || row >= thePuzzle.height
            || col < 0 || col >= thePuzzle.width) {
            return {
                nextRow: origRow,
                nextCol: origCol,
                isHorizontal
            };
        }
        else if (thePuzzle.grid[row][col].isFillable) {
            if (!shouldSkipFilled || thePuzzle.gridValues[row][col] === '') {
                return {
                    nextRow: row,
                    nextCol: col,
                    isHorizontal
                };
            }
        }
        if (!shouldJump && !thePuzzle.grid[row][col].isFillable) {
            return {
                nextRow: origRow,
                nextCol: origCol,
                isHorizontal
            };
        }
    }
}

interface CrosswordContainerProps {
    roomName: string;
    puzzleJson: string;
}

interface CrosswordContainerState {
    thePuzzle: Crossword;
    cursorRow: number;
    cursorCol: number;
    isHorizontal: boolean;
    solved: boolean;
}

class CrosswordContainer extends React.Component<CrosswordContainerProps, CrosswordContainerState> {
    sync: StateSync;

    constructor(props: CrosswordContainerProps) {
        super(props);

        this.state = {
            thePuzzle: null,
            isHorizontal: true,
            cursorRow: 0,
            cursorCol: 0,
            solved: false
        };

        this.sync = new StateSync(
            props.roomName,
            this.onNewRoomCreated,
            this.onInitialStateReceived,
            this.onEventsReceived
        );
        document.addEventListener('keydown', this.handleKeyPress);
    }

    render() {
        if (this.state.thePuzzle) {
            return <div className="cw-main">
                <div className="cw-main-column-1">
                    <CrosswordGrid
                        thePuzzle={this.state.thePuzzle}
                        cursorRow={this.state.cursorRow}
                        cursorCol={this.state.cursorCol}
                        isHorizontal={this.state.isHorizontal}
                        handleClick={this.handleClick}
                    />
                </div>
                <div className="cw-main-column-2">
                    <CrosswordClues
                        thePuzzle={this.state.thePuzzle}
                        cursorRow={this.state.cursorRow}
                        cursorCol={this.state.cursorCol}
                        isHorizontal={this.state.isHorizontal}
                        handleClick={this.handleClickSingleIndex}
                    />
                </div>
            </div>;
        } else {
            return <p style={{ textAlign: 'center', marginTop: '400px', fontSize: '28px' }}>
                Room Name: {this.props.roomName}
                <br />
                Waiting for a puzzle to be pushed to this room...
            </p>;
        }
    }

    handleClick = (row: number, col: number) => {
        if (this.state.thePuzzle.grid[row][col].isFillable) {
            if (row === this.state.cursorRow && col === this.state.cursorCol) {
                this.setState({
                    isHorizontal: !this.state.isHorizontal
                });
            } else {
                this.setState({
                    cursorRow: row,
                    cursorCol: col
                });
            }
        }
    }

    handleClickSingleIndex = (index: number, isHorizontal: boolean) => {
        const row = Math.floor(index / this.state.thePuzzle.width);
        const col = index % this.state.thePuzzle.width;

        if (this.state.thePuzzle.grid[row][col].isFillable) {
            if (row === this.state.cursorRow && col === this.state.cursorCol) {
                this.setState({
                    isHorizontal: !this.state.isHorizontal
                });
            } else {
                this.setState({
                    cursorRow: row,
                    cursorCol: col,
                    isHorizontal
                });
            }
        }
    }

    fillLetterDeferred = (row: number, col: number, c: string) => {
        if (this.state.solved) {
            return;
        }

        this.sync.pushEvent({
            row,
            col,
            c
        });
    }

    onNewRoomCreated = () => {
        if (this.props.puzzleJson) {
            this.sync.pushInitialState(JSON.parse(this.props.puzzleJson));
        }
    }

    onInitialStateReceived = (state: any) => {
        const thePuzzle = new Crossword(state, this.onPuzzleCompleted);
        this.setState({
            thePuzzle,
            isHorizontal: true,
            cursorRow: thePuzzle.startingCell.row,
            cursorCol: thePuzzle.startingCell.col
        });
    }

    onEventsReceived = (evs: Array<{row: number, col: number, c: string}>) => {
        for (const ev of evs) {
            this.state.thePuzzle.fillLetter(
                ev.row,
                ev.col,
                ev.c
            );
        }
        this.forceUpdate();
    }

    onPuzzleCompleted = () => {
        this.setState({
            solved: true
        });
        document.querySelector('body').style.backgroundColor = 'lightgreen';
    }

    handleKeyPress = (event: KeyboardEvent) => {
        let shouldPreventDefault: boolean = true;
        let dir: Direction = null;
        let shouldJump: boolean = true;
        let shouldSkipFilled: boolean = false;

        const wasFilled: boolean = this.state.thePuzzle.getLetter(
            this.state.cursorRow,
            this.state.cursorCol
        ) !== '';

        const keyCode = event.keyCode;
        if (keyCode >= 65 && keyCode <= 90) {
            shouldJump = false;
            const c = String.fromCharCode(keyCode);
            this.fillLetterDeferred(
                this.state.cursorRow,
                this.state.cursorCol,
                c
            );

            if (this.state.isHorizontal) {
                dir = { dx: 0, dy: 1 };
            } else {
                dir = { dx: 1, dy: 0 };
            }

            if (!wasFilled) {
                shouldSkipFilled = true;
            }
        } else {
            switch (keyCode) {
                case 37: // left
                    dir = { dx: 0, dy: -1 };
                    break;
                case 38: // up
                    dir = { dx: -1, dy: 0 };
                    break;
                case 39: // right
                    dir = { dx: 0, dy: 1 };
                    break;
                case 40: // down
                    dir = { dx: 1, dy: 0 };
                    break;
                case 9: // tab
                case 13: // enter
                    const nextClue = this.state.thePuzzle.getNextClue(
                        this.state.cursorRow,
                        this.state.cursorCol,
                        this.state.isHorizontal
                    );
                    if (nextClue) {
                        this.handleClickSingleIndex(nextClue.cell, this.state.isHorizontal);
                    }
                    break;
                case 8: // backspace
                    this.fillLetterDeferred(
                        this.state.cursorRow,
                        this.state.cursorCol,
                        ''
                    );

                    if (!wasFilled) {
                        if (this.state.isHorizontal) {
                            dir = { dx: 0, dy: -1 };
                        } else {
                            dir = { dx: -1, dy: 0 };
                        }

                        this.advanceCursor(dir, false, false);
                        this.fillLetterDeferred(
                            this.state.cursorRow,
                            this.state.cursorCol,
                            ''
                        );

                        dir = null;
                    }
                    break;
                default:
                    shouldPreventDefault = false;
            }
        }

        if (dir !== null) {
            this.advanceCursor(dir, shouldJump, shouldSkipFilled);
        }

        if (true || shouldPreventDefault) {
            event.preventDefault();
        }
    }

    advanceCursor(
        dir: Direction,
        shouldJump: boolean,
        shouldSkipFilled: boolean
    ) {
        const nextState = getNextFillable(
            this.state.cursorRow,
            this.state.cursorCol,
            this.state.isHorizontal,
            this.state.thePuzzle,
            dir,
            shouldJump,
            shouldSkipFilled
        );
        if (nextState.nextRow !== this.state.cursorRow
            || nextState.nextCol !== this.state.cursorCol
            || nextState.isHorizontal !== this.state.isHorizontal) {
            this.setState({
                cursorRow: nextState.nextRow,
                cursorCol: nextState.nextCol,
                isHorizontal: nextState.isHorizontal
            });
        }
    }
}

export { CrosswordContainer };
