import React from 'react';

import { Lobby } from './Lobby';

class App extends React.Component<{}, {}> {
    constructor(props: {}) {
        super(props);
    }

    public componentWillMount() {

    }

    public componentWillUnmount() {

    }

    render() {
        return <Lobby />;
    }
}

export default App;
