import React from 'react';

import { CrosswordContainer } from './CrosswordEditor/CrosswordContainer';

import Dropzone from 'react-dropzone';

interface LobbyState {
    roomName: string;
    puzzleJson: string;
    inLobby: boolean;
}

class Lobby extends React.Component<{}, LobbyState> {
    constructor(props: {}) {
        super(props);

        const params = new URLSearchParams(location.search.slice(1));
        const roomNameParam = params.get('room');
        let roomName = '';
        let inLobby = true;
        if (roomNameParam) {
            roomName = roomNameParam;
            inLobby = false;
        }

        this.state = {
            roomName,
            puzzleJson: '',
            inLobby
        };
    }

    handleRoomNameChange = (ev: any) => {
        this.setState({
            roomName: ev.target.value.trim()
        });
    }

    handlePuzzleJsonChange = (ev: any) => {
        this.setState({
            puzzleJson: ev.target.value
        });
    }

    handleJoinRoom = () => {
        if (this.state.roomName) {
            this.setState({
                inLobby: false
            });
        }
    }

    handleFileDrop = (accepted: File[]) => {
        if (accepted.length) {
            const file = accepted[0];
            const reader = new FileReader();
            reader.onload = () => {
                let fileAsBase64: string = reader.result;
                fileAsBase64 = fileAsBase64.split(',')[1];
                fetch(
                    '/crossword/api/convert_puzzle',
                    { method: 'POST', body: fileAsBase64 }
                ).then((value: Response) => {
                    return value.json();
                }).then((response: string) => {
                    if (response) {
                        this.setState({
                            puzzleJson: JSON.stringify(response)
                        });
                    }
                });
            };
            reader.readAsDataURL(file);
        }
    }

    maybeRenderLobbyForm = () => {
        if (this.state.inLobby) {
            return <div className="lobby-form">
                <input placeholder="Room Name" value={this.state.roomName} onChange={this.handleRoomNameChange} />
                <Dropzone
                    onDrop={this.handleFileDrop}
                    style={{}}
                    disableClick
                >
                    <textarea placeholder="Puzzle JSON" value={this.state.puzzleJson} onChange={this.handlePuzzleJsonChange} />
                </Dropzone>
                <button onClick={this.handleJoinRoom}>Join Room</button>
            </div>;
        }
    }

    maybeRenderCrossword = () => {
        if (!this.state.inLobby) {
            return <CrosswordContainer roomName={this.state.roomName} puzzleJson={this.state.puzzleJson} />;
        }
    }

    render() {
        return <div className="container">
            {this.maybeRenderLobbyForm()}
            {this.maybeRenderCrossword()}
        </div>;
    }
}

export { Lobby };
export default Lobby;
