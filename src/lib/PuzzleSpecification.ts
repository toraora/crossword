export interface PuzzleSpecification {
    width: number;
    height: number;
    fill: string[][];
    markup: string[][];
    clues_h: PuzzleClue[];
    clues_v: PuzzleClue[];
    solution: string;
}

export interface PuzzleClue {
    cell: number;
    num: number;
    len: number;
    clue: string;
}
