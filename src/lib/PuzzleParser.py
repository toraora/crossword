from puz import read, load
import sys
import base64

MARKUP_LOOKUP = dict()
MARKUP_LOOKUP[0] = 'Normal'
MARKUP_LOOKUP[128] = 'Circle'

spec = dict()

inp = sys.stdin.read()

p = load(base64.b64decode(inp))
m = p.markup()
c = p.clue_numbering()

p.unlock_solution(1)

spec['width'] = p.width
spec['height'] = p.height
spec['fill'] = [[p.fill[p.width * x + y] for y in range(p.width)] for x in range(p.height)]
if m.markup:
    spec['markup'] = [[MARKUP_LOOKUP[m.markup[p.width * x + y]] for y in range(p.width)] for x in range(p.height)]
spec['clues_h'] = c.across
spec['clues_v'] = c.down
spec['solution'] = p.solution

import json
print json.dumps(spec)
