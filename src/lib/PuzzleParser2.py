from puz import read, load
import sys

MARKUP_LOOKUP = dict()
MARKUP_LOOKUP[0] = 'Normal'
MARKUP_LOOKUP[128] = 'Circle'

spec = dict()

i = raw_input()
#p = read('/Users/nathan.wong/Downloads/' + i + '.puz')
p = load(sys.stdin.read())
m = p.markup()
c = p.clue_numbering()

p.unlock_solution(1)

spec['width'] = p.width
spec['height'] = p.height
spec['fill'] = [[p.fill[p.width * x + y] for y in range(p.width)] for x in range(p.height)]
spec['markup'] = [[MARKUP_LOOKUP[m.markup[p.width * x + y]] for y in range(p.width)] for x in range(p.height)]
spec['clues_h'] = c.across
spec['clues_v'] = c.down
spec['solution'] = p.solution

import json
print json.dumps(spec)
