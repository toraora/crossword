import { PuzzleSpecification, PuzzleClue } from './PuzzleSpecification';

enum GridEntryType {
   Rebus,
   Normal
}

enum GridEntryDisplayType {
   Normal,
   Shaded,
   Circle
}

interface GridEntry {
    isFillable: boolean;
    type: GridEntryType;
    displayType: GridEntryDisplayType;
    acrossClue?: PuzzleClue;
    downClue?: PuzzleClue;
    clueLabel?: number;
    correctAnswer?: string;
}

class Crossword {
    width: number;
    height: number;
    grid: GridEntry[][];
    gridValues: string[][];

    puzzle: PuzzleSpecification;
    startingCell: { row: number, col: number };

    gridCorrectness: boolean[][];
    numFillable: number;
    numCorrect: number;

    onCompleteCallback: (() => void);

    constructor(puzzle: PuzzleSpecification, onCompleteCallback: (() => void)) {
        this.onCompleteCallback = onCompleteCallback;

        this.width = puzzle.width;
        this.height = puzzle.height;
        this.grid = [];
        this.gridValues = [];
        this.gridCorrectness = [];

        this.numFillable = 0;
        this.numCorrect = 0;

        for (let x = 0; x < this.height; x++) {
            const thisRow = new Array<GridEntry>();
            const thisValueRow = new Array<string>();
            const thisCorrectnessRow = new Array<boolean>();
            this.grid.push(thisRow);
            this.gridValues.push(thisValueRow);
            this.gridCorrectness.push(thisCorrectnessRow);

            for (let y = 0; y < this.width; y++) {
                thisRow.push({
                    isFillable: false,
                    type: GridEntryType.Normal,
                    displayType: GridEntryDisplayType.Normal,
                    acrossClue: null,
                    downClue: null,
                    correctAnswer: null
                });
                thisValueRow.push('');
                thisCorrectnessRow.push(false);
            }
        }

        this.puzzle = puzzle;
        this.startingCell = null;
        this.parsePuzzleSpec(puzzle);
    }

    parsePuzzleSpec(puzzle: PuzzleSpecification) {
        for (let x = 0; x < this.height; x++) {
            for (let y = 0; y < this.width; y++) {
                const thisEntry = this.grid[x][y];
                const thisFill = puzzle.fill[x][y];
                const thisMarkup = puzzle.markup[x][y];

                if (thisFill === '.') {
                    thisEntry.isFillable = false;
                } else {
                    thisEntry.isFillable = true;
                    thisEntry.correctAnswer = puzzle.solution.charAt(this.width * x + y);
                    this.numFillable += 1;
                }

                switch (thisMarkup) {
                    case 'Normal':
                        thisEntry.displayType = GridEntryDisplayType.Normal;
                        break;
                    case 'Circle':
                        thisEntry.displayType = GridEntryDisplayType.Circle;
                        break;
                }
            }
        }

        for (const clue of puzzle.clues_h) {
            const startRow = Math.floor(clue.cell / this.width);
            const startCol = clue.cell % this.width;

            if (this.startingCell === null) {
                this.startingCell = { row: startRow, col: startCol };
            }

            this.grid[startRow][startCol].clueLabel = clue.num;
            for (let i = 0; i < clue.len; i++) {
                const thisEntry = this.grid[startRow][startCol + i];
                thisEntry.acrossClue = clue;
            }
        }

        for (const clue of puzzle.clues_v) {
            const startRow = Math.floor(clue.cell / this.width);
            const startCol = clue.cell % this.width;
            this.grid[startRow][startCol].clueLabel = clue.num;
            for (let i = 0; i < clue.len; i++) {
                const thisEntry = this.grid[startRow + i][startCol];
                thisEntry.downClue = clue;
            }
        }
    }

    fillLetter(row: number, col: number, letter: string) {
        if (this.grid[row][col].isFillable) {
            this.gridValues[row][col] = letter;
            // console.log(`${row} ${col} ${letter} ${this.grid[row][col].correctAnswer} ${this.numCorrect} ${this.numFillable}`);

            if (letter === this.grid[row][col].correctAnswer) {
                this.numCorrect += this.gridCorrectness[row][col] ? 0 : 1;
                this.gridCorrectness[row][col] = true;
                if (this.numCorrect === this.numFillable) {
                    this.onCompleteCallback();
                }
            } else {
                this.numCorrect -= this.gridCorrectness[row][col] ? 1 : 0;
                this.gridCorrectness[row][col] = false;
            }
        }
    }

    getLetter(row: number, col: number) {
        return this.gridValues[row][col];
    }

    getClue(row: number, col: number, isHorizontal: boolean) {
        if (isHorizontal) {
            return this.grid[row][col].acrossClue;
        } else {
            return this.grid[row][col].downClue;
        }
    }

    getNextClue(row: number, col: number, isHorizontal: boolean) {
        if (isHorizontal) {
            const cur = this.grid[row][col].acrossClue;
            const curIdx = this.puzzle.clues_h.indexOf(cur);
            if (curIdx + 1 < this.puzzle.clues_h.length) {
                return this.puzzle.clues_h[curIdx + 1];
            }
        } else {
            const cur = this.grid[row][col].downClue;
            const curIdx = this.puzzle.clues_v.indexOf(cur);
            if (curIdx + 1 < this.puzzle.clues_v.length) {
                return this.puzzle.clues_v[curIdx + 1];
            }
        }

        return null;
    }
}

export { Crossword };
export { GridEntry };
export { GridEntryDisplayType };
