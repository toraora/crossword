
declare var ReconnectingWebSocket: any;

enum StateSyncStatus {
    NO_STATE,
    WAITING_INITIAL,
    READY
}

interface StateSyncMessage {
    method: string;
    payload?: object | string;
}

interface StateSyncEventPush {
    unique_id: string;
    payload: object;
}

interface StateSyncEvent {
    Id: number;
    Payload: string;
}

function randId() {
    return Math.random().toString(36).substr(2, 10);
}

class StateSync {
    name: string;
    status: StateSyncStatus = StateSyncStatus.NO_STATE;
    last_id: number = 0;
    ws: WebSocket;

    onNewRoomCreated: any;
    onInitialStateReceived: any;
    onEventsReceived: any;

    constructor(
        name: string,
        onNewRoomCreated: any,
        onInitialStateReceived: any,
        onEventsReceived: any
    ) {
        this.name = name;
        let protocol = 'wss://';
        let pathPrefix = '/crossword/ws/';
        if (location.host.indexOf('localhost') !== -1) {
            protocol = 'ws://';
            pathPrefix = '/ws/';
        }
        this.ws = new ReconnectingWebSocket(protocol + location.host + pathPrefix + this.name);
        this.ws.onopen = this.handleWebSocketOpen;
        this.ws.onmessage = this.handleMessage;

        this.onNewRoomCreated = onNewRoomCreated;
        this.onInitialStateReceived = onInitialStateReceived;
        this.onEventsReceived = onEventsReceived;
    }

    handleWebSocketOpen = (ev: Event) => {
        // do we care about the event?
        ev = ev;

        if (this.status === StateSyncStatus.NO_STATE) {
            this.sendMessage({
                method: 'INITIAL_REQUEST'
            });
        } else {
            this.sendMessage({
                method: 'MAKE_CURRENT',
                payload: JSON.stringify({
                    last_id: this.last_id
                })
            });
        }
    }

    sendMessage = (msg: StateSyncMessage) => {
        this.ws.send(JSON.stringify(msg));
    }

    handleMessage = (ev: MessageEvent) => {
        const msg: StateSyncMessage = JSON.parse(ev.data);
        switch (msg.method) {
            case 'NO_STATE':
                this.status = StateSyncStatus.WAITING_INITIAL;
                this.onNewRoomCreated();
                break;

            case 'INITIAL_PAYLOAD':
                if (this.status !== StateSyncStatus.READY) {
                    this.status = StateSyncStatus.READY;
                    this.onInitialStateReceived(JSON.parse((msg.payload as any).Payload));
                    this.last_id = (msg.payload as any).Id;
                    this.bringToCurrent();
                }
                break;

            case 'EVENT_UPDATE':
                if (this.status === StateSyncStatus.READY) {
                    const events = msg.payload as StateSyncEvent[];
                    const toPass: object[] = [];
                    for (const event of events) {
                        if (event.Id <= this.last_id) {
                            continue;
                        } else if (event.Id > this.last_id + 1) {
                            this.bringToCurrent();
                            break;
                        }

                        toPass.push(JSON.parse(event.Payload as string).payload);
                        this.last_id = event.Id;
                    }

                    if (toPass) {
                        this.onEventsReceived(toPass);
                    }
                }
                break;
        }
    }

    pushInitialState = (state: object) => {
        this.sendMessage({
            method: 'PUSH_INITIAL_STATE',
            payload: JSON.stringify(state)
        });
    }

    pushEvent = (ev: object) => {
        const eventPushPayload: StateSyncEventPush = {
            unique_id: randId(),
            payload: ev
        };
        this.sendMessage({
            method: 'EVENT_PUSH',
            payload: JSON.stringify(eventPushPayload)
        });
    }

    bringToCurrent = () => {
        this.sendMessage({
            method: 'MAKE_CURRENT',
            payload: JSON.stringify({
                last_id: this.last_id
            })
        });
    }
}

export { StateSync };
